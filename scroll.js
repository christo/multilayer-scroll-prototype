on("mousedown", "tbody > tr", function(event) {
  activate(this);
  event.preventDefault();
});
on("mousemove", "body:not(.details) tbody > tr", function() {
  select(this);
});
on("keydown", "*", function(event) {
  if (this.tagName === "INPUT") {
    return;
  }
  if (event.keyCode === 27) {
    deactivate();
  } else if (event.keyCode === 13 && !document.body.classList.contains("details")) {
    var tr = document.querySelector(".selected");
    if (tr) {
      activate(tr);
    }
  }
});
on("keypress", "*", function(event) {
  if (this.tagName === "INPUT") {
    return;
  }
  var shortcut = String.fromCharCode(event.keyCode);
  var prev = document.querySelector(".selected");
  var tr;
  if (shortcut === "e") {
    if (!document.body.classList.contains("details")) {
      var tr = document.querySelector(".selected");
      if (tr) {
        activate(tr);
      }
    }
    return;
  }
  if (shortcut === "j") {
    tr = (prev)
      ? (prev.nextElementSibling || prev)
      : document.querySelector("tbody > tr");
  } else if (shortcut === "k") {
    tr = (prev)
      ? (prev.previousElementSibling || prev)
      : document.querySelector("tbody > tr:last-child");
  }
  if (!tr) {
    return;
  }
  if (document.body.classList.contains("details")) {
    if (tr !== prev) {
      activate(tr);
    }
  } else {
    if (tr === prev) {
      var y = window.scrollY;
      y += (shortcut === "j") ? 32 : -32;
      window.scrollTo(0, y);
      return;
    }
    select(tr);
    var r = tr.getBoundingClientRect();
    if (r.bottom > window.innerHeight) {
      window.scrollTo(0, window.scrollY + r.bottom - window.innerHeight + 16);
    } else if (r.top < 0) {
      window.scrollTo(0, window.scrollY + r.top - 16);
    }
  }
});
function select(tr) {
  var prev = document.querySelector(".selected");
  if (prev) {
    prev.classList.remove("selected");
  }
  tr.classList.add("selected");
}
var stalker = document.querySelector("#stalker");
var stalking = false;
var fetched = false;
window.onscroll = function() {
  var y = window.scrollY;
  if (stalking) {
    if (y <= 182) {
      stalking = false;
      stalker.classList.remove("detached");
    }
  } else {
    if (y > 182) {
      stalking = true;
      stalker.classList.add("detached");
    }
  }
  if (!fetched && window.scrollY + window.innerHeight > 976) {
    setTimeout(function() {
      var t = document.querySelector("tbody");
      var a = Array.prototype.slice.call(t.rows);
      while (a.length > 0) {
        var i = rand(a.length);
        t.appendChild(a[i].cloneNode(true)).className = "";
        a.splice(i, 1);
      }
      updateMinHeight();
    }, 500);
    fetched = true;
  }
};
updateMinHeight();
function updateMinHeight() {
  var r = document.querySelector("#results").getBoundingClientRect();
  document.querySelector("#details").style.minHeight = (r.bottom - r.top) + "px";
};
var D = document.querySelector("#details");
var P = D.querySelectorAll("p");
function activate(tr) {
  var show = !tr.classList.contains("active");
  deactivate();
  if (show) {
    tr.classList.add("active");
    document.body.className = "details";
    var p = D.querySelectorAll("p");
    for (var i = 0; i < p.length; i++) {
      D.removeChild(p[i]);
    }
    var n = 1 + rand(20);
    for (var i = 0; i < n; i++) {
      D.appendChild(
        P[rand(P.length)].cloneNode(true)
      );
    }
    var a = document.querySelectorAll("#stalker a");
    var k = tr.querySelector(".key");
    a[0].firstChild.data = getProjectName(k.firstChild.data);
    a[1].firstChild.data = k.firstChild.data;
    a[2].firstChild.data = k.parentNode.lastChild.data;
    if (window.scrollY > 75) {
      window.scrollTo(0, 75);
    }
  }
  select(tr);
}
function deactivate() {
  var tr = document.querySelector("tr.active");
  if (tr) {
    tr.classList.remove("active")
  }
  document.body.className = "";
} 
var PROJECTS = {
  "APL": "Application Links",
  "CONF": "Confluence",
  "GH": "GreenHopper",
  "JRADEV": "JIRA Development",
  "PLATFORM": "Atlassian Platform",
  "REST": "Atlassian REST",
  "UI": "Design"
};
function getProjectName(key) {
  return PROJECTS[/^\w*/.exec(key)[0]] || "No Project";
}
function rand(n) {
  return Math.floor(Math.random() * n);
}
function on(type, selector, handler) {
  document.addEventListener(type, function(event) {
    var t = event.target;
    do {
      if (t.webkitMatchesSelector(selector)) {
        handler.call(t, event);
        break;
      }
    } while (t = t.parentElement);
  }, true);
}
